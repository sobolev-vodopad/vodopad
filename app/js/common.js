// Variables
const formWrapper = document.querySelector('.vacancy-form__wrapper'),
      topSlider = document.querySelector('.slider'),
      topSliderSlides = topSlider.querySelectorAll('.slide'),
      topSliderReel = topSlider.querySelector('.slider__reel'),
      vacancy = document.querySelector('.vacancy__info'),
      vacancyControl = vacancy.querySelectorAll('.vacancy__control-list-item'),
      vacancyContent = vacancy.querySelectorAll('.vacancy__content-slide'),
      vacancySliderReel = vacancy.querySelector('.blue-reel'),
      formHeaderButtons = formWrapper.querySelectorAll('.vacancy-form__header-button'),
      formSlides = formWrapper.querySelectorAll('.vacancy-form__change'),
      vacancySteps = formWrapper.querySelectorAll('.vacancy-form__step'),
      vacancyStepsReel = formWrapper.querySelector('.change-nav__reel'),
      select = formWrapper.querySelectorAll('.select'),
      skillsWrapper = formWrapper.querySelector('.vacancy-form__column_skills'),
      cloneButton = formWrapper.querySelectorAll('.clone-button'),
      switches = formWrapper.querySelectorAll('.vacancy-form__switch'),
      vacancyHeaderButtons = formWrapper.querySelectorAll('.vacancy-form__header-button'),
      dragSection = formWrapper.querySelector('.vacancy-form__section_drag');




// Listeners
eventListeners();

function eventListeners() {
    topSlider.addEventListener('click', changeSlides); // Listening to top slider
    vacancy.addEventListener('click', changeSlides); // Listening to changing vacancies in vacancy block
    formWrapper.addEventListener('click', listenClicks); // Listening to click for custom switches + changing form types + changing form slides
}




// Top slider and vacancy-changer functions

// Change slides and tabs in top slider and vacancy block
function changeSlides(event, slider, sliderReel) {
    const el = event.target;

    // If top slider / slider with vacancy
    if (el.classList.contains('slider__control-list-item')) { //If top slider
        slider = topSliderSlides;
        sliderReel = topSliderReel;

    } else if (el.classList.contains('vacancy__control-list-item')) { // If vacancy slider
        slider = vacancyContent;
        sliderReel = vacancySliderReel;

        // Set active attribute to button
        vacancyControl.forEach(function(item) {
            item.removeAttribute('active');
        });
        el.setAttribute('active', '');

    } else {
        return;
    }
    const index = [...el.parentElement.children].indexOf(el);
    setIndex(slider);
    setVerticalReelPosition(sliderReel, index, el);
}




// Set vertical slides tabs reel position
function setVerticalReelPosition(reel, positionIndex, parameterElement) {
    const reelHeight = parameterElement.clientHeight;
    reel.style.top = reelHeight * positionIndex - 1 + 'px';
}


// Set index of clicked element and show/hide indexed slide
function setIndex(indexed) {
    const index = [...event.target.parentElement.children].indexOf(event.target);
    indexed.forEach(function(item) {
        item.removeAttribute('active');
    });
    indexed[index].setAttribute('active', '');
}







// Form functions


// Soft skills functions
skillsWrapper.addEventListener('click', addSkills);

// Add skills function
function addSkills(event) {
    const input = skillsWrapper.querySelector('.vacancy-form__input');

    if (event.target.classList.contains('soft-skills__title')) {
        input.value = input.value + event.target.innerText + ';' + ' ';
        event.target.parentElement.classList.add('hidden');
    }
}


function addListeners(el, func) {
    for (let i = 0; i < el.length; i++) {
        el[i].addEventListener('click', func);
    }
}

addListeners(cloneButton, cloneForm);
addListeners(switches, activateSwitches);
addListeners(vacancyHeaderButtons, changeFormTypes);
// Functions

// Show/hide elements in form + change form types + change form slides
function listenClicks(event) {
    if (event.target.classList.contains('vacancy-form__change-nav-button')) { // Changing of form types
        const el = event.target;
        const index = [...el.parentElement.children].indexOf(event.target);
        setIndex(vacancySteps);
        setHorizontalReelPosition(vacancyStepsReel, index, el);


        const nextStepButton = formWrapper.querySelector('.form-change__button'); // Change form slides when clicked next button

        if (index === 4) {
            nextStepButton.parentElement.style.display = 'none';
        } else {
            nextStepButton.parentElement.style.display = 'block';
        }

    } else if (event.target.classList.contains('select')) { // Activate custom selector
        selector(event);
    } else if (event.target.classList.contains('clone-block__delete-button')) { // Delete cloned form-block
        event.target.parentElement.remove();
    } else {
        return;
    }

}


// Show/hide blocks after custom-switches
function activateSwitches(event) {
    const switchParent = event.target.closest('.custom-switch'),
          eventWrapper = switchParent.nextElementSibling;

    if (eventWrapper.hasAttribute('hidden')) {
        eventWrapper.removeAttribute('hidden');
    } else {
        eventWrapper.setAttribute('hidden','');
    }
}


//    Custom selector
function selector(event) {
    const header = event.target,
          body = header.nextElementSibling;

    if (body.hasAttribute('active') && header.hasAttribute('active')) {
        body.removeAttribute('active');
        header.removeAttribute('active');
    } else {
        body.setAttribute('active', '');
        header.setAttribute('active', '');
    }

    body.onclick = function(event) {
        if (event.target.classList.contains('select__list-option')) {
            header.value = event.target.innerText;

            body.removeAttribute('active');
            header.removeAttribute('active');
            header.setAttribute('selected', '');
        } else {
            return;
        }
    }
}


// Change form types
function changeFormTypes() {
    setIndex(formSlides);
    formHeaderButtons.forEach(function(item){
        item.removeAttribute('active');
    });
    event.target.setAttribute('active', '');

}


// Set horizontal tabs reel position
function setHorizontalReelPosition(reel, positionIndex, parameterElement) {
    const reelWidth = parameterElement.clientWidth;
    reel.style.left = reelWidth * positionIndex + 'px';
}

function nextSlide(event) {
    for (let i = 0; i < vacancySteps.length; i++) {
        if (vacancySteps[i].hasAttribute('active')) {
            vacancySteps[i].removeAttribute('active');
            vacancySteps[++i].setAttribute('active', '');
            vacancyStepsReel.style.left = formWrapper.querySelector('.vacancy-form__change-nav-button').clientWidth * i + 'px';
            if (i === vacancySteps.length - 1) {
                event.target.parentElement.style.display = 'none';
            }
        }
    }
}

// Clone form
function cloneForm(event) {
    const cloneObject = event.target.previousElementSibling.firstElementChild,
          clone = cloneObject.cloneNode(true),
          deleteButton = document.createElement("button"),
          blockBorder = document.createElement("span");

    deleteButton.innerText = '+';
    deleteButton.classList.add('clone-block__delete-button');
    blockBorder.classList.add('clone-block__border');
    clone.classList.add('cloned');

    event.target.previousElementSibling.appendChild(clone);
    event.target.previousElementSibling.lastElementChild.appendChild(deleteButton);
    event.target.previousElementSibling.lastElementChild.appendChild(blockBorder);
}


// Upload file function
function uploadFile(event) {
    const realBtn = event.target.parentElement.firstElementChild;
    realBtn.click();
}

function getFileName(event) {
    if (event.target.value) {
        event.target.nextElementSibling.innerHTML = event.target.value;
        event.target.nextElementSibling.classList.add('chosen');
    } else {
        event.target.nextElementSibling.innerHTML = 'Файл не выбран';
    }
}



// Show blocks when radio checked
const conditionedRadio = formWrapper.querySelectorAll('.custom-radio_conditioned');

for (const radio of conditionedRadio) {
    radio.addEventListener('click', function(event) {

        let parent = (radio.closest('.vacancy-form__row')) ? '.vacancy-form__row' : '.army__wrapper';

        if (radio.hasAttribute('condition')) {
            radio.closest(parent).nextElementSibling.style.display = 'flex';
        } else {
            radio.closest(parent).nextElementSibling.style.display = 'none';
        }

    });
}


// Smooth scroll to anchor
$(".button_blue-header").on("click", function () {
    $("html, body").animate({
        scrollTop: $("#vacancySection").offset().top
    }, 500);
});

// Drag and drop
(function ($) {
    let lastPlace;

    $(".drag-block_front").draggable({
        revert: true,
        zIndex: 999,
        snap: ".drag-place__wrapper",
        snapMode: "inner",
        snapTolerance: 40,
        start: function (event, ui) {
            lastPlace = $(this).parent();
        }
    });

    $(".drag-place__wrapper").droppable({
        drop: function (event, ui) {
            let dropped = ui.draggable,
                droppedOn = this;

            if ($(droppedOn).children().length > 0) {
                $(droppedOn).children().detach().prependTo($(lastPlace));
            }

            $(dropped).detach().css({
                top: 0,
                left: 0,
                zIndex: 999
            }).prependTo($(droppedOn));

            $(droppedOn).css({
                opacity: 1
            })
        }
    });
})(jQuery);
